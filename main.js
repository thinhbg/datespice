window.onload = function () {
      document.getElementById("my-form").onsubmit = function () {
        const emailValue = $("#form-email").val();
        const answerValue = $("#form-answer").val();
        try {
          let data = {"operationName":null,"variables":{},"query":`mutation {\n  CreateSubscriber(Email: \"${emailValue}\", Answer: \"${answerValue}\") {\n    _id\n  }\n}\n`}

          $.ajax({
            method: "POST",
            url:
              "https://eeht0kaudd.execute-api.us-west-2.amazonaws.com/dev/graphql",
            data: JSON.stringify(data),
            dataType: "json",
            processData: false,
            error: function (msg) {
              console.log("The result =", msg);
            },
            success: function (msg) {
              console.log("The result =", msg);
            },
          });
        } catch (error) {
        }
        return false;
      };
    };
